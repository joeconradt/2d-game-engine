﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace _2DGameEngine
{
    class Camera
    {
        public Vector2 Position = new Vector2();

        public int Mode
        {
            get;
            set;
        }
        public GraphicsDeviceManager Graphics
        {
            get;
            set;
        }
        public int Speed
        {
            get;
            set;
        }
        public GameObject Target
        {
            get;
            set;
        }

        public Camera(GraphicsDeviceManager graphics)
        {
            Graphics = graphics;
            Position = new Vector2(0, 0);
            Mode = 0;
            Speed = 10;
        }
        public Camera(GraphicsDeviceManager graphics, Vector2 position)
        {
            Graphics = graphics;
            Position = position;
            Mode = 0;
            Speed = 10;
        }
        public Camera(GraphicsDeviceManager graphics, Vector2 position, int mode)
        {
            Graphics = graphics;
            Position = position;
            Mode = mode;
            Speed = 10;
        }
        public Camera(GraphicsDeviceManager graphics, Vector2 position, int mode, int speed)
        {
            Graphics = graphics;
            Position = position;
            Mode = mode;
            Speed = speed;
        }
        public void Update(KeyboardState ks)
        {
            if (Mode == 0)
            {

            }
            else if (Mode == 1)
            {
                if (ks.IsKeyDown(Keys.W))
                {
                    Position.Y -= Speed;
                }
                if (ks.IsKeyDown(Keys.A))
                {
                    Position.X -= Speed;
                }
                if (ks.IsKeyDown(Keys.S))
                {
                    Position.Y += Speed;
                }
                if (ks.IsKeyDown(Keys.D))
                {
                    Position.X += Speed;
                }
            }
            else if (Mode == 2)
            {
                Position.X = Target.Position.X - (Graphics.PreferredBackBufferWidth / 2); //+ (Target.Position.Width/2);
                Position.Y = Target.Position.Y - (Graphics.PreferredBackBufferHeight / 2); //+ (Target.Position.Height/2);
            }
        }
        public void Attach(GameObject gameObject)
        {
            Mode = 2;
            Target = gameObject;
        }
    }
}
