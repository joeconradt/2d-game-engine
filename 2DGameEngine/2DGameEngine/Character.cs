﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace _2DGameEngine
{
    class Character : GameElement
    {
        public bool isJumping = false;
        private float maxJumpSpeed = 15;
        public float jumpSpeed = 0;

        public int Health
        {
            get;
            set;
        }

        public Character(GraphicsDeviceManager graphics, Texture2D sprite, Rectangle position, int gravity, int speed)
            : base(graphics, sprite, position, gravity, speed)
        {
            
        }
        public override void Update(KeyboardState ks, GamePadState gps)
        {
            if (isJumping)
            {
                Position.Y -= Convert.ToInt32(jumpSpeed);
                if (jumpSpeed > 0)
                    jumpSpeed -= 0.3f;
                else
                    jumpSpeed = 0;
            }

            Position.Y += Gravity;
            base.Update(ks,gps);
        }
        public virtual void MoveLeft()
        {
            Position.X -= Speed;
        }
        public virtual void MoveRight()
        {
            Position.X += Speed;
        }
        public void Jump()
        {
            if (!isJumping)
            {
                isJumping = true;
                jumpSpeed = maxJumpSpeed;
            }
        }
        public override void CollisionBottom(GameObject go)
        {
            if(!go.overrideCollision)
            isJumping = false;
            base.CollisionBottom(go);
        }
    }
}
