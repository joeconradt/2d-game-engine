﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace _2DGameEngine
{
    class GameElement : GameObject
    {
        static private int frameRate = 0;
        private int currentFrame = frameRate;
        public int Speed
        {
            get;
            set;
        }
        public int Gravity
        {
            get;
            set;
        }
        public GameElement(GraphicsDeviceManager graphics, Texture2D sprite, Rectangle position, int gravity, int speed)
            : base(graphics, sprite, position)
        {
            Speed = speed;
            Gravity = gravity;
        }
        public override void Update(KeyboardState ks, GamePadState gps)
        {
            base.Update(ks, gps);
        }
        public virtual void CollisionLeft(GameObject go)
        {
            if(!go.overrideCollision)
            this.Position.X = go.Position.X + go.Position.Width;
        }
        public virtual void CollisionRight(GameObject go)
        {
            if (!go.overrideCollision)
            this.Position.X = go.Position.X - this.Position.Width;
        }
        public virtual void CollisionTop(GameObject go)
        {
            if (!go.overrideCollision)
            this.Position.Y = go.Position.Y + go.Position.Height;
        }
        public virtual void CollisionBottom(GameObject go)
        {
            if (!go.overrideCollision)
            this.Position.Y = go.Position.Y - this.Position.Height;
        }
        public virtual void Collision(GameElement ge)
        {

        }
    }
}
