using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;
using System.IO;

namespace _2DGameEngine
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        public int LOADTIME = 0;
        public int MENU_SPEED = 60;

        public Boolean PlayingLevel = false;

        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        SplashScreen splashScreen;

        public KeyboardState KeyboardState;
        public MouseState MouseState;

        Texture2D guySprite;

        private Level level1;
        private Preloader preloader;
        private DrawableGameComponent ToLoad;

        List<String> mainMenuLevel1 = new List<String>() { "Play", "Level Editor", "Other", "Quit" };
        List<String> playMenuStrings = new List<String>() { "Story", "Survival", "My Levels", "- Back to Main" };

        GamerServicesComponent gsc;

        public AudioManagement music;

        /// <summary>
        ///  Constructor for game
        /// </summary>
        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        /// <summary>
        /// Set the window size to how ever big the screen size is
        /// </summary>
        protected override void Initialize()
        {
            //graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferMultiSampling = true;
            graphics.ApplyChanges();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // my stuff
            spriteBatch = new SpriteBatch(GraphicsDevice);

            splashScreen = new SplashScreen(this, GraphicsDevice, spriteBatch, Content);

            //level1 = new Level(this, GraphicsDevice, spriteBatch, Content, "Content\\level1.txt");

            guySprite = Content.Load<Texture2D>("images/guy");

            Components.Add(splashScreen);
            splashScreen.Complete += new EventHandler(OnSplashScreenComplete);
            splashScreen.Start();

            music = new AudioManagement(Content);
            

            // SKIP TO MENU
            //MainMenu();
        }
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            if (!PlayingLevel)
            {
                GraphicsDevice.Clear(Color.White);
                spriteBatch.Begin();
                spriteBatch.Draw(guySprite, new Rectangle(this.Window.ClientBounds.Width / 2 - guySprite.Width / 2, this.Window.ClientBounds.Height / 2 - guySprite.Height / 2, guySprite.Width, guySprite.Height), Color.White);
                spriteBatch.End();
            }

            base.Draw(gameTime);
        }
        public void MainMenu()
        {
            PlayingLevel = false;
            RemoveAllComponents();
            GameMenu mainMenu = new GameMenu(this, GraphicsDevice, graphics, spriteBatch, Content, mainMenuLevel1, new Vector2(this.Window.ClientBounds.Width / 2 - 100, this.Window.ClientBounds.Height / 2), 50);
            mainMenu.OnSelect += new EventHandler(MainMenuSelectEvent);
        }
        private void MainMenuSelectEvent(object sender, EventArgs eventArgs)
        {
            String menuItem = ((GameMenu)sender).GetCurrentItem();
            ((GameMenu)sender).Destroy();
            if (menuItem.Equals("Play"))
            {
                GameMenu playMenu = new GameMenu(this, GraphicsDevice, graphics, spriteBatch, Content, playMenuStrings, new Vector2(this.Window.ClientBounds.Width / 2 - 100, this.Window.ClientBounds.Height / 2), 50);
                playMenu.OnSelect += new EventHandler(PlayMenuSelectEvent);
            }
            else if (menuItem.Equals("Level Editor"))
            {
                LevelEditor levelEditor = new LevelEditor(this, GraphicsDevice, graphics, spriteBatch, Content);
                Preload(levelEditor);
            }
            else if (menuItem.Equals("Quit"))
            {
                this.Exit();
            }
        }
        private void PlayMenuSelectEvent(object sender, EventArgs eventArgs)
        {
            String menuItem = ((GameMenu)sender).GetCurrentItem();
            ((GameMenu)sender).Destroy();
            if (menuItem.Equals("Story"))
            {

            }
            else if (menuItem.Equals("Survival"))
            {

            }
            else if (menuItem.Equals("My Levels"))
            {
                String[] levelFilePaths = Directory.GetFiles("Content\\custom_levels");
                List<String> myLevelsMenuStrings = new List<String>();
                foreach (String str in levelFilePaths)
                {
                    myLevelsMenuStrings.Add(str.Split('\\')[str.Split('\\').Length - 1]);
                }
                myLevelsMenuStrings.Add("- Back to Main");
                GameMenu myLevelsMenu = new GameMenu(this, GraphicsDevice, graphics, spriteBatch, Content, myLevelsMenuStrings, new Vector2(this.Window.ClientBounds.Width / 2 - 100, this.Window.ClientBounds.Height / 2), 50);
                myLevelsMenu.OnSelect += new EventHandler(MyLevelsMenuSelectEvent);
            }
            else if (menuItem.Equals("- Back to Main"))
            {
                MainMenu();
            }
        }
        private void MyLevelsMenuSelectEvent(object sender, EventArgs eventArgs)
        {
            String menuItem = ((GameMenu)sender).GetCurrentItem();
            ((GameMenu)sender).Destroy();
            if (menuItem.Equals("- Back to Main"))
            {
                MainMenu();
            }
            else
            {
                RemoveAllComponents();
                Components.Add(new Level(this, GraphicsDevice, spriteBatch, Content, "Content\\custom_levels\\" + menuItem));
            }
        }
        private void OnSplashScreenComplete(object sender, EventArgs eventArgs)
        {
            splashScreen.Dispose();
            RemoveAllComponents();
            Preload(null);
        }
        public void Preload(DrawableGameComponent ToLoad)
        {
            this.ToLoad = ToLoad;
            preloader = new Preloader(this, GraphicsDevice, spriteBatch, Content);
            Components.Add(preloader);
            preloader.Start();
            preloader.Complete += new EventHandler(OnPreloaderComplete);
        }
        public void OnPreloaderComplete(object sender, EventArgs eventArgs)
        {
            RemoveAllComponents();
            preloader.Dispose();
            if (ToLoad != null)
            {
                Components.Add(ToLoad);
            }
            else
            {
                MainMenu();
            }
        }
        public void RemoveAllComponents()
        {
            while (Components.Count > 0)
            {
                ((DrawableGameComponent)Components[0]).Dispose();
            }
        }
    }
}
