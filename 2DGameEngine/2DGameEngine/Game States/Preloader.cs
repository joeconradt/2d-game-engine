using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace _2DGameEngine
{
    public class Preloader : Microsoft.Xna.Framework.DrawableGameComponent
    {

        SpriteBatch spriteBatch;
        SpriteFont spriteFont;
        Game game;
        GraphicsDevice graphics;

        Texture2D ring1;
        Texture2D ring2;
        Texture2D ring3;
        Texture2D ring4;
        Rectangle ring1_rect;
        Rectangle ring2_rect;
        Rectangle ring3_rect;
        Rectangle ring4_rect;
        float ring1Rotation;
        float ring2Rotation;
        float ring3Rotation;
        float ring4Rotation;

        public event EventHandler Complete;

        Random rand = new Random();
        int interval;
        int count;

        Boolean running = false;

        public Preloader(Game game, GraphicsDevice graphics, SpriteBatch spriteBatch, ContentManager Content): base(game)
        {
            this.spriteBatch = spriteBatch;
            spriteFont = Content.Load<SpriteFont>("spriteFonts/loading");
            ring1 = Content.Load<Texture2D>("images/ring_1");
            ring2 = Content.Load<Texture2D>("images/ring_2");
            ring3 = Content.Load<Texture2D>("images/ring_3");
            ring4 = Content.Load<Texture2D>("images/ring_4");
            this.game = game;            
            this.graphics = graphics;
            ring1_rect = new Rectangle((game.Window.ClientBounds.Width / 2), (game.Window.ClientBounds.Height / 2), ring1.Width, ring1.Height);
            ring2_rect = new Rectangle((game.Window.ClientBounds.Width / 2), (game.Window.ClientBounds.Height / 2), ring2.Width, ring2.Height);
            ring3_rect = new Rectangle((game.Window.ClientBounds.Width / 2), (game.Window.ClientBounds.Height / 2), ring3.Width, ring3.Height);
            ring4_rect = new Rectangle((game.Window.ClientBounds.Width / 2), (game.Window.ClientBounds.Height / 2), ring4.Width, ring4.Height);

            interval = game.LOADTIME + rand.Next(0, 100);
        }
        public override void Initialize()
        {

            base.Initialize();
        }
        protected override void LoadContent()
        {
            ring1Rotation = 0;
            ring2Rotation = 0;
            ring3Rotation = 0;
            ring4Rotation = 0;
            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            if (running)
            {
                count++;
                if(count >= interval)
                {
                    OnComplete();
                }
                ring1Rotation += 0.02f;
                ring2Rotation -= 0.01f;
                ring3Rotation += 0.04f;
                ring4Rotation -= 0.05f;
            }
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            if (running)
            {
                graphics.Clear(Color.Black);
                spriteBatch.Begin();
                spriteBatch.Draw(ring1, ring1_rect, null, Color.White, ring1Rotation, new Vector2(ring1.Width / 2, ring1.Height / 2), SpriteEffects.None, 0f);
                spriteBatch.Draw(ring2, ring2_rect, null, Color.White, ring2Rotation, new Vector2(ring2.Width / 2, ring2.Height / 2), SpriteEffects.None, 0f);
                spriteBatch.Draw(ring3, ring3_rect, null, Color.White, ring3Rotation, new Vector2(ring3.Width / 2, ring3.Height / 2), SpriteEffects.None, 0f);
                spriteBatch.Draw(ring4, ring4_rect, null, Color.White, ring4Rotation, new Vector2(ring4.Width / 2, ring4.Height / 2), SpriteEffects.None, 0f);
                spriteBatch.DrawString(spriteFont, "Loading...", new Vector2((game.Window.ClientBounds.Width / 2) - spriteFont.MeasureString("Loading...").X / 2, (game.Window.ClientBounds.Height / 2) + 200), Color.White);
                spriteBatch.End();
            }
            base.Draw(gameTime);
        }
        public void Start()
        {
            running = true;
        }
        protected virtual void OnComplete()
        {
            EventHandler handler = Complete;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}
