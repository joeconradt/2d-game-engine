using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;


namespace _2DGameEngine
{
    public class SplashScreen : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Game game;
        GraphicsDevice graphics;
        ContentManager Content;
        GraphicsDeviceManager graphicsDeviceManager;

        SpriteFont pressEnterFont;

        Texture2D splashScreenTexture;
        Texture2D coverArtTexutre;
        float splashScreenAlpha;
        int pressStartAlpha = 0;
        Boolean fadeOut;
        Boolean pressStartFadeOut = false;

        Boolean running = false;
        Boolean splash = true;

        bool firstTime = true;

        public event EventHandler Complete;

        public SplashScreen(Game game, GraphicsDevice graphics, SpriteBatch spriteBatch, ContentManager Content): base(game)
        {
            this.game = game;
            this.graphics = graphics;
            this.spriteBatch = spriteBatch;
            this.Content = Content;
            this.graphicsDeviceManager = game.graphics;

            splashScreenTexture = Content.Load<Texture2D>("images/hawks");
            coverArtTexutre = Content.Load<Texture2D>("images/dark_project");
            pressEnterFont = Content.Load<SpriteFont>("spriteFonts/press_start");
            splashScreenAlpha = -70;
        }
        public override void Initialize()
        {

            base.Initialize();
        }
        public override void Update(GameTime gameTime)
        {
            if (running)
            {
                if (splash)
                {
                    if (fadeOut)
                    {
                        splashScreenAlpha -= 2f;
                    }
                    else
                    {
                        splashScreenAlpha += 2f;
                    }
                    if (splashScreenAlpha >= 400)
                    {
                        fadeOut = true;
                    }
                    if (splashScreenAlpha < -70 && fadeOut)
                    {
                        splash = false;
                        splashScreenAlpha = 0;
                    }
                }
                else
                {
                    if (firstTime)
                    {
                        game.music.PlayBackgroundMusic(0);
                        firstTime = false;
                    }
                    if (splashScreenAlpha < 255)
                    {
                        splashScreenAlpha += 2f;
                    }
                    else
                    {
                        if (!pressStartFadeOut)
                        {
                            pressStartAlpha += 2;
                            if (pressStartAlpha > 255)
                            {
                                pressStartFadeOut = true;
                            }
                        }
                        else
                        {
                            pressStartAlpha -= 2;
                            if (pressStartAlpha < 0)
                            {
                                pressStartFadeOut = false;
                            }
                        }
                        if (Keyboard.GetState().IsKeyDown(Keys.Enter))
                        {
                            OnComplete();
                        }
                    }
                }
            }
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            graphics.Clear(Color.Black);
            spriteBatch.Begin();
            if (splash)
            {
                spriteBatch.Draw(splashScreenTexture, new Rectangle(0, 0, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height), new Color((int)splashScreenAlpha, (int)splashScreenAlpha, (int)splashScreenAlpha, 255));
            }
            else
            {
                spriteBatch.Draw(coverArtTexutre, new Rectangle(0, 0, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height), new Color((int)splashScreenAlpha, (int)splashScreenAlpha, (int)splashScreenAlpha, 255));
                if (splashScreenAlpha >= 255)
                {
                    spriteBatch.DrawString(pressEnterFont, "PRESS START", new Vector2(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / 2 -50, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height / 2 - 100), new Color(pressStartAlpha, pressStartAlpha, pressStartAlpha));
                }
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
        public void Start()
        {
            running = true;
        }
        protected virtual void OnComplete()
        {
            EventHandler handler = Complete;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}
