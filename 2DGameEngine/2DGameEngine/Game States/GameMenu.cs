﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace _2DGameEngine
{
    class GameMenu : DrawableGameComponent 
    {
        Game game;
        GraphicsDevice graphics;
        GraphicsDeviceManager graphicsManager;
        SpriteBatch spriteBatch;
        ContentManager Content;

        public event EventHandler OnSelect;

        SpriteFont menuFont;

        KeyboardState oldState;

        Vector2 position = new Vector2(0,0);
        Vector2 targetPosition;
        Color backgroundColor = Color.Pink;
        int spacing;
        public int animate = 0;

        int currentIndex = 0;

        Texture2D whitePixel;

        Color inactiveColor = Color.White;
        Color activeColor = Color.CornflowerBlue;

        private List<String> MasterMenu = new List<String>();

        public GameMenu(Game game, GraphicsDevice graphics, GraphicsDeviceManager graphicsManager, SpriteBatch spriteBatch, ContentManager Content, List<String> MasterMenu, Vector2 position, int spacing) : base(game)
        {
            this.game = game;
            this.graphics = graphics;
            this.spriteBatch = spriteBatch;
            this.Content = Content;
            this.graphicsManager = graphicsManager;
            this.targetPosition = position;
            this.MasterMenu = MasterMenu;
            this.spacing = spacing;

            this.position.Y = targetPosition.Y;
            this.position.X = game.Window.ClientBounds.Width;

            game.Components.Add(this);

            menuFont = Content.Load<SpriteFont>("spriteFonts/menu");
            whitePixel = Content.Load<Texture2D>("images/whitePixel");
        }
        protected override void LoadContent()
        {
            
            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            if (animate == 0)
            {
                position.X -= game.MENU_SPEED;
                if (position.X < targetPosition.X)
                {
                    animate = 1;
                    position.X = targetPosition.X;
                }
            }
            else if (animate == 1)
            {
                if (Keyboard.GetState() != oldState)
                {
                    KeyboardState ks = Keyboard.GetState();
                    if (ks.IsKeyDown(Keys.W))
                    {
                        MoveUp();
                    }
                    if (ks.IsKeyDown(Keys.S))
                    {
                        MoveDown();
                    }
                    if (ks.IsKeyDown(Keys.Enter))
                    {
                        animate = 2;
                    }
                }
                oldState = Keyboard.GetState();
            }
            else if(animate == 2)
            {
                position.X -= game.MENU_SPEED;
                if (position.X < -200)
                {
                    animate = 3;
                    Select();
                }
            }
            base.Update(gameTime);
        }
        public void AddBackground(Color backgroundColor)
        {
            this.backgroundColor = backgroundColor;
        }
        public void ChangeColors(Color inactiveColor, Color activeColor)
        {
            this.inactiveColor = inactiveColor;
            this.activeColor = activeColor;
        }
        public void MoveUp()
        {
            currentIndex--;
            if (currentIndex < 0)
            {
                currentIndex = 0;
            }
        }
        public void MoveDown()
        {
            currentIndex++;
            if (currentIndex > MasterMenu.Count -1)
            {
                currentIndex = MasterMenu.Count - 1;
            }
        }
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            if (backgroundColor != Color.Pink)
            {
                spriteBatch.Draw(whitePixel, new Rectangle((int)targetPosition.X - 30, 0, 150, 2000), backgroundColor);
            }
            int counter = 0;
            foreach (String str in MasterMenu)
            {
                Color color = inactiveColor;
                if (counter == currentIndex)
                {
                    color = activeColor;
                }
                spriteBatch.DrawString(menuFont, str, new Vector2(position.X, position.Y - (currentIndex * spacing) + (counter * spacing)), color);
                counter++;
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
        protected virtual void Select()
        {
            EventHandler handler = OnSelect;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
        public String GetCurrentItem()
        {
            return MasterMenu[currentIndex];
        }
        public void Destroy()
        {
            game.Components.Remove(this);
            Dispose();
        }
    }
}
