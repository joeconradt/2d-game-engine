﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Diagnostics;

namespace _2DGameEngine
{
    class Level : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Game game;
        GraphicsDevice graphics;
        ContentManager Content;
        GraphicsDeviceManager graphicsDeviceManager;

        Texture2D tileSprite;
        Texture2D whitePixel;
        Texture2D front_buildingsSprite;
        Texture2D back_buildingsSprite;


        MouseState mouseState;
        MouseState previousMouseState;

        Tile player_spawnTile;

        int y = 250;

        SpriteFont menuSpriteFont;

        Camera Camera;
        float oldCamera;

        Boolean Paused = false;

        String levelFile;

        int enemyCount = 0;
        int enemyMax = 10;

        List<GameObject> gameObjects = new List<GameObject>();
        List<GameElement> gameElements = new List<GameElement>();
        List<GameObject> visibleObjects = new List<GameObject>();
        List<GameObject> lampObjects = new List<GameObject>();
        List<GameObject> enemySpawner = new List<GameObject>();

        List<Rectangle2> frontPositions = new List<Rectangle2>();
        List<Rectangle2> backPositions = new List<Rectangle2>();

        List<String> menuStrings = new List<String>() { "Resume", "Quit" };

        Player player;

        public Level(Game game, GraphicsDevice graphics, SpriteBatch spriteBatch, ContentManager Content, String levelFile)
            : base(game)
        {
            this.game = game;
            this.graphics = graphics;
            this.spriteBatch = spriteBatch;
            this.Content = Content;
            this.graphicsDeviceManager = game.graphics;
            this.levelFile = levelFile;
            game.PlayingLevel = true;
        }
        public override void Initialize()
        {
            base.Initialize();
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            this.game.music.PlayBackgroundMusic(1);

            #region load textures
            tileSprite = Content.Load<Texture2D>("images/tiles");
            front_buildingsSprite = Content.Load<Texture2D>("images/front_buildings");
            back_buildingsSprite = Content.Load<Texture2D>("images/back_buildings");
            whitePixel = new Texture2D(GraphicsDevice, 1, 1);
            whitePixel.SetData(new Color[] { Color.White });
            #endregion

            Camera = new Camera(graphicsDeviceManager);
            oldCamera = Camera.Position.X;

            backPositions.Add(new Rectangle2(-200, y, back_buildingsSprite.Width, front_buildingsSprite.Height));
            backPositions.Add(new Rectangle2(400, y, back_buildingsSprite.Width, front_buildingsSprite.Height));
            backPositions.Add(new Rectangle2(1000, y, back_buildingsSprite.Width, front_buildingsSprite.Height));
            backPositions.Add(new Rectangle2(1600, y, back_buildingsSprite.Width, front_buildingsSprite.Height));

            frontPositions.Add(new Rectangle2(-200, y, back_buildingsSprite.Width, front_buildingsSprite.Height));
            frontPositions.Add(new Rectangle2(400, y, back_buildingsSprite.Width, front_buildingsSprite.Height));
            frontPositions.Add(new Rectangle2(1000, y, back_buildingsSprite.Width, front_buildingsSprite.Height));
            frontPositions.Add(new Rectangle2(1600, y, back_buildingsSprite.Width, front_buildingsSprite.Height));

            LevelLoader ll = new LevelLoader(graphicsDeviceManager, tileSprite, levelFile);
            gameObjects.AddRange(ll.Load());
            foreach (Tile tile_ in gameObjects)
            {
                if (tile_.isLight)
                {
                    AddLamp(tile_);
                }
                if (tile_.Attribute == "player_spawn")
                {
                    player_spawnTile = tile_;
                }
                else if (tile_.Attribute == "enemy_spawn")
                {
                    enemySpawner.Add(tile_);
                }
            }

            player = new Player(graphicsDeviceManager, Content.Load<Texture2D>("images/Ryan"), new Rectangle((int)player_spawnTile.Position.X, (int)player_spawnTile.Position.Y - 70, 36, 70), 10, 5);
            gameElements.Add(player);
            menuSpriteFont = Content.Load<SpriteFont>("spriteFonts/menu");

            Camera.Attach(player);

            UpdateVisibleObjects();
        }
        public override void Update(GameTime gameTime)
        {
            if (!Paused)
            {
                if (new Random().Next(200) == 199)
                {
                    SpawnEnemy();
                }
                mouseState = Mouse.GetState();
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                    game.Exit();
                if (Keyboard.GetState().IsKeyDown(Keys.D1))
                {
                    Camera.Mode = 1;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Delete))
                    game.Exit();
                if(Keyboard.GetState().IsKeyDown(Keys.Escape))
                {
                    ShowMenu();
                }
                if (Keyboard.GetState().IsKeyDown(Keys.P))
                {
                    SpawnEnemy();
                }

                #region camera click attach
                if (mouseState.LeftButton != previousMouseState.LeftButton)
                {
                    if (mouseState.LeftButton == ButtonState.Pressed)
                    {
                        foreach (GameObject go in gameObjects)
                        {
                            if (new Rectangle(Convert.ToInt32(go.Position.X - Camera.Position.X), Convert.ToInt32(go.Position.Y - Camera.Position.Y), go.Position.Width, go.Position.Height).Contains(mouseState.X, mouseState.Y))
                            {
                                //Camera.Attach(go);
                                AddLamp(go);
                            }
                        }
                    }
                }
                #endregion

                List<GameElement> deadZombies = new List<GameElement>();
                foreach(GameElement gameElement in gameElements)
                {
                    RenderLight(gameElement);
                    gameElement.Update(Keyboard.GetState(), GamePad.GetState(PlayerIndex.One));
                    foreach (GameElement hitElement in gameElements)
                    {
                        if (!gameElement.Equals(hitElement) && gameElement.Position.Intersects(hitElement.Position))
                        {
                            gameElement.Collision(hitElement);
                        }
                    }
                    if (gameElement is Enemy)
                    {
                        if (((Enemy)gameElement).state.Equals("dead"))
                        {
                            deadZombies.Add(gameElement);
                        }
                    }
                }
                foreach (GameElement deadZombie in deadZombies)
                {
                    gameElements.Remove(deadZombie);
                }
                foreach (GameObject gameObject in gameObjects)
                {
                    gameObject.isVisible = CheckVisibility(gameObject);
                    RenderLight(gameObject);
                    foreach (GameElement gameElement in gameElements)
                    {
                        if (gameObject.Position.Intersects(gameElement.Position))
                        {
                            if (gameElement.Position.Y + gameElement.Position.Height <= gameObject.Position.Y + 11)
                            {
                                gameElement.CollisionBottom(gameObject);
                            }
                            else if (gameElement.Position.Y >= gameObject.Position.Y + gameObject.Position.Height)
                            {
                                gameElement.CollisionTop(gameObject);
                            }
                            else if (gameElement.Position.X <= gameObject.Position.X - gameElement.Position.Width + 6)
                            {
                                gameElement.CollisionRight(gameObject);
                            }
                            else if (gameElement.Position.X >= gameObject.Position.X + gameObject.Position.Width - 6)
                            {
                                gameElement.CollisionLeft(gameObject);
                            }
                        }
                    }
                }

                visibleObjects.Clear();

                MoveParallaxLeft((Camera.Position.X - oldCamera) / 5);
                oldCamera = Camera.Position.X;
                Camera.Update(Keyboard.GetState());
                previousMouseState = Mouse.GetState();
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();
            #region parallax
            foreach (Rectangle2 rect in backPositions)
            {
                spriteBatch.Draw(back_buildingsSprite, new Rectangle((int)rect.X, (int)rect.Y, (int)rect.Width, (int)rect.Height), Color.White);
            }
            foreach (Rectangle2 rect in frontPositions)
            {
                spriteBatch.Draw(front_buildingsSprite, new Rectangle((int)rect.X, (int)rect.Y, (int)rect.Width, (int)rect.Height), Color.White);
            }
            #endregion
            #region draw all game objects
            foreach (GameObject go in gameObjects)
            {
                if (go.Sprite != null && go.isVisible)
                {
                    Color color = go.Tint;
                    if(Paused)
                    {
                        color = new Color(go.Tint.R-50, go.Tint.G-50, go.Tint.B-50);
                    }
                    spriteBatch.Draw(go.Sprite, new Rectangle(Convert.ToInt32(go.Position.X - Camera.Position.X), Convert.ToInt32(go.Position.Y - Camera.Position.Y), go.Position.Width, go.Position.Height), go.Source, go.Tint, 0, Vector2.Zero, go.spriteEffects, 0);
                }
            }
            foreach (GameElement ge in gameElements)
            {
                Color color = ge.Tint;
                if (ge is Enemy)
                {
                    if (((Enemy)ge).isRunner)
                    {
                        color = new Color(color.R + 100, color.G, color.B);
                    }
                }
                spriteBatch.Draw(ge.Sprite, new Rectangle(Convert.ToInt32(ge.Position.X - Camera.Position.X), Convert.ToInt32(ge.Position.Y - Camera.Position.Y), ge.Position.Width, ge.Position.Height), ge.Source, color, 0, Vector2.Zero, ge.spriteEffects, 0);
            }
            #endregion
            if (Paused)
            {

            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void RenderLight(GameObject gameObject)
        {
            gameObject.Lightness = 0;
            foreach (GameObject lamp in lampObjects)
            {
                if (lamp.isLight)
                {
                    int distance = (int)Vector2.Distance(new Vector2(lamp.Position.X, lamp.Position.Y), new Vector2(gameObject.Position.X, gameObject.Position.Y));
                    gameObject.Lightness += Math.Max(0, 255 - distance);
                }
            }
        }
        private bool CheckVisibility(GameObject gameObject)
        {
            return new Rectangle((int)Camera.Position.X - 25, (int)Camera.Position.Y - 25, graphicsDeviceManager.PreferredBackBufferWidth + 50, graphicsDeviceManager.PreferredBackBufferHeight + 50).Contains(gameObject.Position);
        }
        private void UpdateVisibleObjects()
        {
            foreach (GameObject gameObject in gameObjects)
            {
                gameObject.isVisible = CheckVisibility(gameObject);
                if (gameObject.isVisible)
                {
                    if (!visibleObjects.Contains(gameObject))
                    {
                        visibleObjects.Add(gameObject);
                    }
                }
                else
                {
                    if (visibleObjects.Contains(gameObject))
                    {
                        visibleObjects.Remove(gameObject);
                    }
                }
            }
        }
        private void AddLamp(GameObject gameObject)
        {
            lampObjects.Add(gameObject);
            gameObject.isLight = true;
        }
        private void ShowMenu()
        {
            Paused = true;
            Vector2 vec = new Vector2(game.Window.ClientBounds.Width / 2, game.Window.ClientBounds.Height / 2);
            GameMenu menu = new GameMenu(game, GraphicsDevice, graphicsDeviceManager, spriteBatch, Content, menuStrings, vec, 50);
            menu.AddBackground(Color.White);
            menu.ChangeColors(Color.Black, Color.DarkBlue);
            menu.OnSelect += new EventHandler(LevelMenuSelectEvent);
        }
        private void LevelMenuSelectEvent(object sender, EventArgs eventArgs)
        {
            String menuItem = ((GameMenu)sender).GetCurrentItem();
            ((GameMenu)sender).Destroy();
            if(menuItem.Equals("Resume"))
            {
                Paused = false;
            }
            else if (menuItem.Equals("Quit"))
            {
                game.MainMenu();
                this.game.music.PlayBackgroundMusic(0);
            }
        }
        private void SpawnEnemy()
        {
            UpdateVisibleObjects();
            List<GameObject> spawnTiles = new List<GameObject>();
            if (enemyCount < enemyMax)
            {
                try
                {
                    foreach (GameObject visibleObject in visibleObjects)
                    {
                        if (visibleObject.Lightness < 20)
                        {
                            GameObject oneAbove = visibleObjects.Find(x => x.Position.Y == visibleObject.Position.Y - Tile.TILE_HEIGHT);
                            GameObject twoAbove = visibleObjects.Find(x => x.Position.Y == visibleObject.Position.Y - (Tile.TILE_HEIGHT * 2));
                            if (oneAbove == null || oneAbove.overrideCollision && twoAbove == null || twoAbove.overrideCollision)
                            {
                                spawnTiles.Add(visibleObject);
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
            if (spawnTiles.Count != 0)
            {
                GameObject spawnTile = spawnTiles[new Random().Next(spawnTiles.Count)];
                Enemy enemy = new Enemy(graphicsDeviceManager, Content.Load<Texture2D>("images/zombie"), new Rectangle(spawnTile.Position.X, spawnTile.Position.Y, 34, 64), 10, 1, player);
                enemy.Position.Y -= enemy.Position.Height;
                gameElements.Add(enemy);
            }
            spawnTiles.Clear();
        }
        public void MoveParallaxLeft(float amount)
        {
            foreach (Rectangle2 rect in frontPositions)
            {
                rect.X -= amount;
                if (rect.X <= -800)
                {
                    frontPositions[0].X = -200;
                    frontPositions[1].X = 400;
                    frontPositions[2].X = 1000;
                    frontPositions[3].X = 1600;
                }
            }
            foreach (Rectangle2 rect in backPositions)
            {
                rect.X -= amount / 3;
                if (rect.X <= -800)
                {
                    backPositions[0].X = -200;
                    backPositions[1].X = 400;
                    backPositions[2].X = 1000;
                    backPositions[3].X = 1600;
                }
            }
        }
        public void MoveParallaxRight(float amount)
        {
            foreach (Rectangle2 rect in frontPositions)
            {
                rect.X += amount;
                if (rect.X >= 1600)
                {
                    frontPositions[0].X = -200;
                    frontPositions[1].X = 400;
                    frontPositions[2].X = 1000;
                    frontPositions[3].X = 1600;
                }
            }
            foreach (Rectangle2 rect in backPositions)
            {
                rect.X += amount / 3;
                if (rect.X >= 1600)
                {
                    backPositions[0].X = -200;
                    backPositions[1].X = 400;
                    backPositions[2].X = 1000;
                    backPositions[3].X = 1600;
                }
            }
        }
    }
}
