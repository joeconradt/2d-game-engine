﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace _2DGameEngine
{
    class Enemy : Character
    {
        public String state = "idle";
        String oldState = "";
        Boolean attacking = true;
        Player player;
        static Random random = new Random();
        public bool isRunner = false;

        public Enemy(GraphicsDeviceManager graphics, Texture2D sprite, Rectangle position, int gravity, int speed, Player player) : base(graphics, sprite, position, gravity, speed)
        {
            this.player = player;
            ChangeCounter(10);
            if ((int)random.Next(3) == 1)
            {
                ChangeCounter(5);
                isRunner = true;
            }
        }
        public override void Update(KeyboardState ks, GamePadState gps)
        {
            if (isRunner)
            {
                Color = new Color(255, Color.G, Color.B);
            }
            if (Position.Intersects(player.Position))
            {
                if (player.swinging)
                {
                    state = "dead";
                }
                else
                {
                    state = "idle";
                    player.Health -= 1;
                }
            }
            if (attacking)
            {
                if (player.Position.X < Position.X)
                {
                    state = "left";
                }
                else if (player.Position.X > Position.X)
                {
                    state = "right";
                }
            }
            if (oldState != state)
            {
                if (state.Equals("left"))
                {
                    
                    animateObject(0, 7, 0, 34, 64, SpriteEffects.None);
                }
                else if (state.Equals("right"))
                {
                    
                    animateObject(0, 7, 0, 34, 64, SpriteEffects.FlipHorizontally);
                }
                else if (state.Equals("idle"))
                {
                    if (random.Next(2) == 1)
                    {
                        animateObject(0, 0, 0, 34, 64, SpriteEffects.FlipHorizontally);
                    }
                    else
                    {
                        animateObject(0, 0, 0, 34, 64, SpriteEffects.None);
                    }
                }
            }
            if (state.Equals("left"))
            {
                MoveLeft();
            }
            else if (state.Equals("right"))
            {
                MoveRight();
            }

            if (random.Next(200) == 199)
            {
                attacking = true;
            }
            if (random.Next(500) == 499)
            {
                attacking = false;
                state = "idle";
            }
            Debug.WriteLine(state);
            oldState = state;
            base.Update(ks, gps);
        }
        public override void CollisionLeft(GameObject go)
        {
            if (!go.overrideCollision)
            {
                state = "right";
                attacking = false;
            }
            base.CollisionLeft(go);
        }
        public override void CollisionRight(GameObject go)
        {
            if (!go.overrideCollision)
            {
                state = "left";
                attacking = false;
            }
            base.CollisionRight(go);
        }
    }
}
