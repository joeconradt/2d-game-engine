﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace _2DGameEngine
{
    class Player : Character
    {
        KeyboardState oldKeyBoardState;
        bool oldJumpingState;
        GamePadState oldGamePadState;
        public bool swinging = false;
        public Player(GraphicsDeviceManager graphics, Texture2D sprite, Rectangle position, int gravity, int speed)
            : base(graphics, sprite, position, gravity, speed)
        {

        }
        public override void Update(KeyboardState ks, GamePadState gps)
        {
            if (ks.IsKeyDown(Keys.Space))
            {
                swinging = true;
            }
            else
            {
                swinging = false;
            }
            //////////////////NOT JUMPING////////////////////
            if (!isJumping && isJumping != oldJumpingState)
            {
                if (ks.IsKeyDown(Keys.A))
                {
                    animateObject(4, 7, 1, 41, 70, SpriteEffects.None);
                }
                if (ks.IsKeyDown(Keys.D))
                {
                    animateObject(0, 3, 1, 41, 70, SpriteEffects.None);
                }
            }

            /////////////JUMPING//////////////
            if (ks.IsKeyDown(Keys.W))//CHECKS IF THE W IS PRESSED
            {
                 if (oldKeyBoardState.IsKeyDown(Keys.W))//IF THE OLD KEY STATE W IS PRESSED
                 {
                     if (isJumping)//IF CHARACTER IS JUMPING
                     {
                         if (oldKeyBoardState.IsKeyDown(Keys.A))//IF MOVING LEFT
                         {
                             animateObject(0, 1, 2, 45, 70, SpriteEffects.FlipHorizontally);//starting x frame, ending x frame, starting y frame, width, height, sprite effect.
                             oldKeyBoardState = ks;
                         }
                         if (oldKeyBoardState.IsKeyDown(Keys.D))//IF MOVING RIGHT
                         {
                                animateObject(0, 1, 2, 45, 70, SpriteEffects.None);
                                oldKeyBoardState = ks;
                         }
                         if (oldKeyBoardState.IsKeyUp(Keys.A) && oldKeyBoardState.IsKeyUp(Keys.D) && oldKeyBoardState.IsKeyUp(Keys.Space))
                         {
                             animateObject(0, 1, 2, 45, 70, SpriteEffects.None);
                             oldKeyBoardState = ks;
                         }
                     }
                 Jump();
                }
            }
            ////////////////////////MOVING LEFT//////////////////////
            if (ks.IsKeyDown(Keys.A))//IF THE A IS PRESSED
            {
                if (!oldKeyBoardState.IsKeyDown(Keys.A))//IF THE OLD 'A' KEY IS NOT DOWN 
                {
                    if (!isJumping)//IF IS NOT JUMPING
                    {
                        animateObject(4, 7, 1, 41, 70, SpriteEffects.None);//AMINATE LEFT WALKING
                    }
                }
                MoveLeft();
            }
            ////////////////////MOVING RIGHT//////////////////
            if (ks.IsKeyDown(Keys.D))
            {
                if (!oldKeyBoardState.IsKeyDown(Keys.D))//IF  OLDKEYBORADSTATE DOESNT = D 
                {
                    if (!isJumping)//IF IS NOT JUMPING
                    {
                        animateObject(0, 3, 1,  41, 70, SpriteEffects.None);//AMINATE RIGHT WALKING
                    }
                }
                MoveRight();
            }
            //////////IDLE////////////
            if (oldKeyBoardState != ks)
            {
                if (ks.IsKeyUp(Keys.A) && ks.IsKeyUp(Keys.D) && ks.IsKeyUp(Keys.W) && ks.IsKeyUp(Keys.Space) && ks.IsKeyUp(Keys.W))//IF LEFT, RIGHT, JUMP, AND SPACEBAR ARE UP
                {
                    animateObject(0, 5, 0, 36, 70, SpriteEffects.None);//IDLE ANIMATION
                }
            }
                ///////////ATTACKING//////////////////
            
                if (ks.IsKeyDown(Keys.Space))
                {
                    if (oldKeyBoardState.IsKeyDown(Keys.D))//IF MOVING RIGHT
                    {
                        animateObject(0, 1, 5, 68, 70, SpriteEffects.None);//ATTACK RIGHT
                        oldKeyBoardState = ks;
                    }
                    if (oldKeyBoardState.IsKeyDown(Keys.A))//IF MOVING LEFT
                    {
                        animateObject(0, 1, 5, 68, 70, SpriteEffects.FlipHorizontally);//ATTACK LEFT
                        oldKeyBoardState = ks;
                    }
                    if (ks.IsKeyUp(Keys.A) && ks.IsKeyUp(Keys.D) && ks.IsKeyUp(Keys.W))//IF IDLE AND ATTACKING
                    {
                        if (oldKeyBoardState != ks)
                        {
                            animateObject(0, 1, 5, 68, 70, SpriteEffects.None);
                        }
                    }
                }
///////////////////////////////////////////////CONTROLLER NOT JUMPING//////////////////////////////////////////////////////////////////////////////
                if (!isJumping && isJumping != oldJumpingState)
                {
                    if (gps.ThumbSticks.Left.X < -0.3)
                    {
                        animateObject(4, 7, 1, 41, 70, SpriteEffects.None);
                    }
                    if (gps.ThumbSticks.Left.X > 0.3)
                    {
                        animateObject(0, 3, 1, 41, 70, SpriteEffects.None);
                    }
                }
////////////////////////////////////////////////CONTROLLER JUMPING/////////////////////////////////////////////////////////////////////////////////
            if (gps.IsButtonDown(Buttons.A))//CHECKS IF THE W IS PRESSED
                {
                    if (oldGamePadState.IsButtonDown(Buttons.A))//IF THE OLD GAMEPAD STATE A IS DOWN
                    {
                        if (isJumping)//IF PLAYER IS JUMPING
                        {
                            if (oldGamePadState.ThumbSticks.Left.X < -0.3)//IF MOVING LEFT
                            {
                                animateObject(0, 1, 2, 45, 70, SpriteEffects.FlipHorizontally);//starting x frame, ending x frame, starting y frame, width, height, sprite effect.
                                oldGamePadState = gps;
                            }
                            if (oldGamePadState.ThumbSticks.Left.X > -0.3)//IF MOVING RIGHT
                            {
                                animateObject(0, 1, 2, 45, 70, SpriteEffects.None);
                                oldGamePadState = gps;
                            }
                            if (!(oldGamePadState.ThumbSticks.Left.X < -0.3) && !(oldGamePadState.ThumbSticks.Left.X < -0.3) && oldGamePadState.IsButtonUp(Buttons.RightTrigger))
                            {
                                animateObject(0, 1, 2, 45, 70, SpriteEffects.None);
                                oldGamePadState = gps;
                            }
                        }
                        Jump();
                    }
                }
////////////////////////////////////////////////////////////CONTROLLER MOVE LEFT//////////////////////////////////////////////////////////
                if (gps.ThumbSticks.Left.X < -0.3)
                {
                    if (!(oldGamePadState.ThumbSticks.Left.X < -0.3))//IF THE OLD THUMBSTICK BUTTON IS PUSHED 
                    {
                        if (!isJumping)//IF IS NOT JUMPING
                        {
                            animateObject(4, 7, 1, 41, 70, SpriteEffects.None);//AMINATE LEFT WALKING
                        }
                    }
                    MoveLeft();
                }


///////////////////////////////////////////////////////////CONTROLLER MOVE RIGHT///////////////////////////////////////////////////////////////////
                if (gps.ThumbSticks.Left.X > 0.3)
                {
                    if (!(oldGamePadState.ThumbSticks.Left.X > 0.3))//IF  OLDKEYBORADSTATE DOESNT = D 
                    {
                        if (!isJumping)//IF IS NOT JUMPING
                        {
                            animateObject(0, 3, 1, 41, 70, SpriteEffects.None);//AMINATE RIGHT WALKING
                        }
                    }
                    MoveRight();
                }
////////////////////////////////////////////////////CONTROLLER IDLE///////////////////////////////////////////////////////////////////////////////
                if (oldGamePadState != gps)
                {
                    if (!(gps.ThumbSticks.Left.X > 0.3) && !(gps.ThumbSticks.Left.X < -0.3) && gps.IsButtonUp(Buttons.A) && gps.IsButtonUp(Buttons.RightTrigger))
                    {
                        animateObject(0, 5, 0, 36, 70, SpriteEffects.None);//IDLE ANIMATION
                    }
                }
////////////////////////////////////////////////CONTROLLER ATTACKING//////////////////////////////////////////////////////////////////////////////
                if (gps.IsButtonDown(Buttons.RightTrigger))
                {
                        if (oldGamePadState.ThumbSticks.Left.X > 0.3)//IF MOVING RIGHT
                        {
                            if (oldGamePadState != gps)
                            {
                                animateObject(0, 1, 5, 68, 70, SpriteEffects.None);//ATTACK RIGHT
                                //oldGamePadState = gps;
                            }
                        }
                        if (oldGamePadState.ThumbSticks.Left.X < -0.3)//IF MOVING LEFT
                        {
                            if (oldGamePadState != gps)
                            {
                                animateObject(0, 1, 5, 68, 70, SpriteEffects.FlipHorizontally);//ATTACK LEFT
                                //oldGamePadState = gps;
                            }
                        }
                        if (!(gps.ThumbSticks.Left.X > 0.3) && !(gps.ThumbSticks.Left.X < -0.3) && gps.IsButtonUp(Buttons.A))
                        {
                            if (oldGamePadState != gps)
                            {
                                animateObject(0, 1, 5, 68, 70, SpriteEffects.None);
                            }
                        }
                }

                oldGamePadState = gps;
                oldKeyBoardState = ks;
                oldJumpingState = isJumping;
                base.Update(ks,gps);
        }
    }
}