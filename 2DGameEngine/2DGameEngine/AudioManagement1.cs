﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.VisualBasic.Devices;
using Microsoft.Xna.Framework.Audio;

namespace _2DGameEngine
{
    public class AudioManagement
    {
        public ContentManager Content;
        SoundEffect song0;
        SoundEffect song1;
        SoundEffect song2;
        SoundEffect song3;
        SoundEffectInstance instance0;
        SoundEffectInstance instance1;
        SoundEffectInstance instance2;
        SoundEffectInstance instance3;
        public AudioManagement(ContentManager Content)
        {
            this.Content = Content;
            song0 = Content.Load<SoundEffect>("sounds/song0");
            song1 = Content.Load<SoundEffect>("sounds/song1");
            song2 = Content.Load<SoundEffect>("sounds/song2");
            song3 = Content.Load<SoundEffect>("sounds/song3");
            instance0 = song0.CreateInstance();
            instance1 = song1.CreateInstance();
            instance2 = song2.CreateInstance();
            instance3 = song3.CreateInstance();

            instance0.IsLooped = true;
            instance1.IsLooped = true;
            instance2.IsLooped = true;
            instance3.IsLooped = true;
        }
        public void PlayBackgroundMusic(int song)
        {
            instance0.Stop();
            instance1.Stop();
            instance2.Stop();
            instance3.Stop();

            if (song == 0)
            {
                instance0.Play();
            }
            if (song == 1)
            {
                instance1.Play();
            }
            if (song == 2)
            {
                instance2.Play();
            }
            if (song == 3)
            {
                instance3.Play();
            }
        }
    }
}
