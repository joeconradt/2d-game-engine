﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _2DGameEngine
{
    class LevelLoader
    {
        private String LevelPath;

        public GraphicsDeviceManager Graphics
        {
            get;
            set;
        }
        public Texture2D Sprite
        {
            get;
            set;
        }

        public LevelLoader(GraphicsDeviceManager graphics, Texture2D tileSprite, String filePath)
        {
            this.LevelPath = filePath;
            Graphics = graphics;
            Sprite = tileSprite;
        }
        public List<GameObject> Load()
        {
            List<GameObject> list = new List<GameObject>();
            int row = 0;
            using(StreamReader reader = new StreamReader(LevelPath))
            {
                String line;
                while((line = reader.ReadLine()) != null)
                {
                    line.TrimEnd(',');
                    string[] tiles = line.Split(',');
                    for (int i = 0; i < tiles.Length; i++)
                    {
                        String tileAttr = "none";
                        if (!tiles[i].Equals("") && !tiles[i].Equals("-1"))
                        {
                            int n;
                            if (!int.TryParse(tiles[i][0].ToString(), out n))
                            {
                                tileAttr = tiles[i][0].ToString();
                                tiles[i] = tiles[i].Substring(1);
                            }
                            int val = Int32.Parse(tiles[i]);
                            Tile tile = new Tile(Graphics, Sprite, new Rectangle(i * Tile.TILE_WIDTH, row * Tile.TILE_HEIGHT, Tile.TILE_WIDTH, Tile.TILE_HEIGHT), val, false, -1);
                            if (val >= 0)
                            {
                                list.Add(tile);
                            }
                            if (val == 8 || val == 9 || val == 10 || val == 11 || val == 12 || val == 13 || val >= 27)
                            {
                                tile.overrideCollision = true;
                            }
                            if (tileAttr.Equals("l"))
                            {
                                tile.isLight = true;
                            }
                            if (tileAttr.Equals("p"))
                            {
                                tile.Attribute = "player_spawn";
                            }
                            if (tileAttr.Equals("e"))
                            {
                                tile.Attribute = "enemy_spawn";
                            }
                        }
                    }
                    row++;
                }
            }
            return list;
        }
    }
}
