﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace _2DGameEngine
{
    class Tile : SolidObject
    {
        public static int TILE_WIDTH = 25;
        public static int TILE_HEIGHT = 25;
        public int TileID
        {
            get;
            set;
        }
        public String Attribute
        {
            get;
            set;
        }
        public Tile(GraphicsDeviceManager graphics, Texture2D sprite, Rectangle position, int tileId, bool destructible, int hitPoints) 
            : base(graphics, sprite, position)
        {
            TileID = tileId;
            Destructible = destructible;
            HitPoints = hitPoints;
            Source = new Rectangle(tileId * TILE_WIDTH, 0, TILE_WIDTH, TILE_HEIGHT);
            if (TileID == 2)
            {
                overrideCollision = true;
            }
        }
    }
}
