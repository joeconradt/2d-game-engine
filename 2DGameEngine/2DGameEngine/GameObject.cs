﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using Microsoft.Xna.Framework.Input;

namespace _2DGameEngine
{
    class GameObject
    {

        //Texture2D animation;
       // Rectangle animRectangle;

        int currentFrameX;
        int currentFrameY;
        int startingFrameX;
        int endingFrameX;
        int startingFrameY;
        int counter = 20;
        int resetCounter;
        public SpriteEffects spriteEffects;

        public GraphicsDeviceManager Graphics
        {
            get;
            set;
        }
        public Texture2D Sprite
        {
            get;
            set;
        }
        public Rectangle Source
        {
            get;
            set;
        }

        public Rectangle Position;
            
        public Color Color
        {
            get;
            set;
        }

        public int Lightness
        {
            get;
            set;
        }

        public Color Tint
        {
            get
            {
                return new Color(Lightness,Lightness,Lightness);
            }
        }

        public bool isVisible
        {
            get;
            set;
        }
        public bool isLight
        {
            get;
            set;
        }
        public bool overrideCollision
        {
            get;
            set;
        }
        public GameObject(GraphicsDeviceManager graphics, Texture2D sprite, Rectangle position)
        {
            Graphics = graphics;
            Sprite = sprite;
            Position = position;
            Source = Position;
            Color = Color.White;
            resetCounter = counter;
        }
        public void ChangeCounter(int num)
        {
            counter = num;
            resetCounter = num;
        }
        public void ChangeColor(Color color)
        {
            Color = color;
        }
        public void animateObject(int startingFrameX, int endingFrameX, int startingFrameY, int frameWidth, int frameHeight, SpriteEffects spriteEffects)
        {
            this.startingFrameX = startingFrameX;
            this.endingFrameX = endingFrameX;
            this.startingFrameY = startingFrameY;
            currentFrameX = startingFrameX;
            currentFrameY = startingFrameY;
            Position.Width = frameWidth;
            Position.Height = frameHeight;
            counter = -1;
            this.spriteEffects = spriteEffects;
        }
        public virtual void Update(KeyboardState ks, GamePadState gps)
        {
            counter--;
            if (counter < 0)
            {
                if (currentFrameX > endingFrameX)
                {
                    currentFrameX = startingFrameX;
                }
                Source = new Rectangle(currentFrameX * Position.Width, currentFrameY * Position.Height, Position.Width, Position.Height);
                currentFrameX++;
                counter = resetCounter;
            }
        }
    }
}
