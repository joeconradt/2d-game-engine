using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace _2DGameEngine
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D tileSprite;

        MouseState mouseState;
        MouseState previousMouseState;

        Camera Camera;

        List<GameObject> gameObjects = new List<GameObject>();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        protected override void Initialize()
        {
            IsMouseVisible = true;
            base.Initialize();
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            #region load textures
            tileSprite = Content.Load<Texture2D>("images/tiles");
            #endregion

            Camera = new Camera(graphics);

            #region testing
            LevelLoader ll = new LevelLoader(graphics, tileSprite, "content/level1.txt");
            gameObjects.AddRange(ll.Load());
            #endregion

            Debug.WriteLine(gameObjects[0].Sprite);

            gameObjects.Add(new Player(graphics, tileSprite, new Rectangle(50, -500, 25, 25), 10, 5));

            Camera.Attach(gameObjects[gameObjects.Count - 1]);
        }
        protected override void UnloadContent()
        {

        }
        protected override void Update(GameTime gameTime)
        {
            mouseState = Mouse.GetState();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            if (Keyboard.GetState().IsKeyDown(Keys.D1))
            {
                Camera.Mode = 1;
            }

            #region camera click attach
            if (mouseState.LeftButton != previousMouseState.LeftButton)
            {
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    foreach (GameObject go in gameObjects)
                    {
                        if (new Rectangle(Convert.ToInt32(go.Position.X - Camera.Position.X),Convert.ToInt32(go.Position.Y - Camera.Position.Y), go.Position.Width, go.Position.Height).Contains(mouseState.X,mouseState.Y))
                        {
                            Camera.Attach(go);
                        }
                    }
                }
            }
            #endregion

            foreach (GameObject gameObject in gameObjects)
            {
                if (gameObject is GameElement)
                {
                    foreach(GameObject hitObject in gameObjects)
                    {
                        if (!hitObject.Equals(gameObject))
                        {
                            if (gameObject.Position.Intersects(hitObject.Position))
                            {
                                gameObject.Position.Y -= ((GameElement)gameObject).Gravity;
                                ((Character)gameObject).isJumping = false;
                            }
                        }
                    }
                }
                if (gameObject is Character)
                {
                    ((Character)gameObject).Update(Keyboard.GetState());
                }
            }

            Camera.Update(Keyboard.GetState());
            previousMouseState = Mouse.GetState();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            #region draw all game objects
            foreach (GameObject go in gameObjects)
            {
                if (go.Sprite != null)
                {
                    spriteBatch.Draw(go.Sprite, new Rectangle(Convert.ToInt32(go.Position.X - Camera.Position.X), Convert.ToInt32(go.Position.Y - Camera.Position.Y), go.Position.Width, go.Position.Height), go.Source, go.Color);
                }
            }
            #endregion
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
