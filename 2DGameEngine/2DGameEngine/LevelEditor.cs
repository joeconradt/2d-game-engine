﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using System.IO;

namespace _2DGameEngine
{
    class LevelEditor : Microsoft.Xna.Framework.DrawableGameComponent
    {
        Game game;
        GraphicsDevice graphics;
        GraphicsDeviceManager graphicsManager;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;
        ContentManager Content;

        Texture2D tileSprite;
        Texture2D outlineSprite;
        Texture2D toolbarSprite;
        Texture2D toolbar_sideSprite;
        Texture2D lampSprite;
        Texture2D originSprite;
        Texture2D player_spawnSprite;
        Texture2D enemy_spawnSprite;

        KeyboardState oldState;

        String headerString = "Choose block";
        String errorString = "";

        Vector2 menuPosition;

        Camera camera;

        List<String> menuStrings = new List<String>() { "Block", "Load", "Save", "Exit", "- Back" };
        List<String> blockMenuStrings = new List<String>() { "Add Lamp", "Remove Lamp", "Player Spawn", "Enemy Spawn", "- Back" };

        List<GameObject> tiles = new List<GameObject>();

        string state = "choose";
        string loadedLevelName = "";
        int currentBlockId = 0;
        int numOfBlocks = 0;
        Vector2 currentPosition;

        public LevelEditor(Game game, GraphicsDevice graphics, GraphicsDeviceManager graphicsManager, SpriteBatch spriteBatch, ContentManager Content) : base(game)
        {
            this.game = game;
            this.graphics = graphics;
            this.spriteBatch = spriteBatch;
            this.Content = Content;
            this.graphicsManager = graphicsManager;

            camera = new Camera(graphicsManager);
            camera.Position.X = -game.Window.ClientBounds.Width / 2 - 25;
            camera.Position.Y = -game.Window.ClientBounds.Height / 2 - 25;

            tileSprite = Content.Load<Texture2D>("images/tiles");
            outlineSprite = Content.Load<Texture2D>("images/outline");
            toolbarSprite = Content.Load<Texture2D>("images/toolbar");
            spriteFont = Content.Load<SpriteFont>("spriteFonts/levelEditor");
            toolbar_sideSprite = Content.Load<Texture2D>("images/toolbar_side");
            lampSprite = Content.Load<Texture2D>("images/lamp");
            originSprite = Content.Load<Texture2D>("images/origin");
            player_spawnSprite = Content.Load<Texture2D>("images/player_spawn");
            enemy_spawnSprite = Content.Load<Texture2D>("images/enemy_spawn");
            currentPosition = new Vector2(0, 0);
            menuPosition = new Vector2(5, game.Window.ClientBounds.Height / 2);
            GetBlocks();

            this.game.music.PlayBackgroundMusic(2);
        }
        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                camera.Position.X -= camera.Speed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                camera.Position.X += camera.Speed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                camera.Position.Y -= camera.Speed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                camera.Position.Y += camera.Speed;
            }
            if(Keyboard.GetState() != oldState)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                {
                    List<GameComponent> gcs = new List<GameComponent>();
                    foreach(GameComponent gc in game.Components)
                    {
                        if (gc is GameMenu)
                        {
                            gcs.Add(gc);
                        }
                    }
                    foreach(GameComponent gc in gcs)
                    {
                        game.Components.Remove(gc);
                    }
                    Menu();
                    state = "menu";
                }
                if (state.Equals("choose"))
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.D))
                    {
                        currentBlockId++;
                        if (currentBlockId > numOfBlocks-1)
                        {
                            currentBlockId = numOfBlocks-1;
                        }
                        UpdateString();
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.A))
                    {
                        currentBlockId--;
                        if (currentBlockId < 0)
                        {
                            currentBlockId = 0;
                        }
                        UpdateString();
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.Enter))
                    {
                        state = "place";
                    }
                }
                else if (state.Equals("place"))
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.A))
                    {
                        currentPosition.X -= Tile.TILE_WIDTH;
                        errorString = "";
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.D))
                    {
                        currentPosition.X += Tile.TILE_WIDTH;
                        errorString = "";
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.W))
                    {
                        currentPosition.Y -= Tile.TILE_HEIGHT;
                        errorString = "";
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.S))
                    {
                        currentPosition.Y += Tile.TILE_HEIGHT;
                        errorString = "";
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.Enter))
                    {
                        AddBlock();
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.R))
                    {
                        RemoveBlock();
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.B))
                    {
                        state = "choose";
                    }
                }
            }
            oldState = Keyboard.GetState();
 	        base.Update(gameTime);
        }
        private void GetBlocks()
        {
            numOfBlocks = tileSprite.Width / Tile.TILE_WIDTH;
        }
        public override void Draw(GameTime gameTime)
        {
            graphics.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            #region draw tiles and attribute icons
            foreach (Tile tile in tiles)
            {
                spriteBatch.Draw(tile.Sprite, new Rectangle((int)tile.Position.X - (int)camera.Position.X, (int)tile.Position.Y - (int)camera.Position.Y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT), tile.Source, Color.White);
                if (tile.isLight)
                {
                    
                    spriteBatch.Draw(lampSprite, new Rectangle((int)tile.Position.X - (int)camera.Position.X, (int)tile.Position.Y - (int)camera.Position.Y, lampSprite.Width, lampSprite.Height), Color.White);
                }
                if (tile.Attribute == "player_spawn")
                {
                    spriteBatch.Draw(player_spawnSprite, new Rectangle((int)tile.Position.X - (int)camera.Position.X, (int)tile.Position.Y - (int)camera.Position.Y, player_spawnSprite.Width, player_spawnSprite.Height), Color.White);
                }
                else if (tile.Attribute == "enemy_spawn")
                {
                    spriteBatch.Draw(enemy_spawnSprite, new Rectangle((int)tile.Position.X - (int)camera.Position.X, (int)tile.Position.Y - (int)camera.Position.Y, player_spawnSprite.Width, player_spawnSprite.Height), Color.White);
                }
            }
            #endregion
            #region origin
            spriteBatch.Draw(originSprite, new Rectangle(-(int)camera.Position.X - 25, -(int)camera.Position.Y - 25, originSprite.Width, originSprite.Height), Color.White);
            #endregion
            #region toolbar sprites
            spriteBatch.Draw(toolbarSprite, new Rectangle(0, 0, game.Window.ClientBounds.Width, toolbarSprite.Height), Color.White);
            spriteBatch.Draw(toolbar_sideSprite, new Rectangle(0, 0, toolbar_sideSprite.Width, game.Window.ClientBounds.Height + 200), Color.White);
#endregion
            // draw temp tile
            spriteBatch.Draw(tileSprite, new Rectangle((int)currentPosition.X - (int)camera.Position.X, (int)currentPosition.Y - (int)camera.Position.Y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT), new Rectangle(currentBlockId * Tile.TILE_WIDTH, 0, Tile.TILE_WIDTH, Tile.TILE_HEIGHT), Color.White);
            // draw tile sheet to pick from
            spriteBatch.Draw(tileSprite, new Rectangle((game.Window.ClientBounds.Width / 2) - (currentBlockId * Tile.TILE_WIDTH), 50, tileSprite.Width, tileSprite.Height), Color.White);
            #region outline
            if (state.Equals("choose"))
            {
                spriteBatch.Draw(outlineSprite, new Rectangle((game.Window.ClientBounds.Width / 2) - 3, 50 - 3, 31, 31), Color.White);
            }
            else
            {
                spriteBatch.Draw(outlineSprite, new Rectangle((int)currentPosition.X - 3 - (int)camera.Position.X, (int)currentPosition.Y - 3 - (int)camera.Position.Y, 31, 31), Color.White);
            }
            #endregion
            #region strings
            spriteBatch.DrawString(spriteFont, headerString, new Vector2((game.Window.ClientBounds.Width / 2) - spriteFont.MeasureString(headerString).X / 2, 5), Color.White);
            spriteBatch.DrawString(spriteFont, errorString, new Vector2(game.Window.ClientBounds.Width / 2 - spriteFont.MeasureString(errorString).X / 2, game.Window.ClientBounds.Height / 2), Color.Red);
            #endregion
            spriteBatch.End();
            base.Draw(gameTime);
        }
        private void AddBlock()
        {
            Tile newTile = new Tile(graphicsManager, tileSprite, new Rectangle((int)currentPosition.X, (int)currentPosition.Y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT), currentBlockId, false, 0);
            if (tiles.Count > 0)
            {
                int counter = 0;
                int flag = -1;
                foreach (Tile tile in tiles)
                {
                    if (tile.Position.X == currentPosition.X && tile.Position.Y == currentPosition.Y)
                    {
                        flag = counter;
                    } 
                    counter++;
                }
                if (flag != -1)
                {
                    tiles[flag] = newTile;
                }
                else
                {
                    tiles.Add(newTile);
                }
            }
            else
            {
                tiles.Add(newTile);
            }
        }
        private void RemoveBlock()
        {
            int flag = -1;
            int counter = 0;
            foreach (Tile tile in tiles)
            {
                if (tile.Position.X == currentPosition.X && tile.Position.Y == currentPosition.Y)
                {
                    flag = counter;
                    break;
                }
                counter++;
            }
            if (flag != -1)
            {
                tiles.RemoveAt(flag);
            }
        }
        private void UpdateString()
        {
            Tile tempTile = new Tile(graphicsManager, tileSprite, new Rectangle((int)currentPosition.X, (int)currentPosition.Y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT), currentBlockId, false, 0);
            if (tempTile.overrideCollision)
            {
                headerString = "Current block: Backing: true";
            }
            else
            {
                headerString = "Current block: Backing: false";
            }
        }
        private object SaveLevel(String levelName)
        {
            bool flag = false;
            foreach (Tile tile in tiles)
            {
                if (tile.Attribute == "player_spawn")
                {
                    flag = true;
                }
            }
            if (!flag)
            {
                errorString = "Level must have a player spawn block";
                state = "place";
                return null;
            }
            String path = "Content\\custom_levels";
            String fileName = levelName + ".txt";
            String combined = path + "\\" + fileName;

            if (!Directory.Exists("Content\\custom_levels"))
            {
                Directory.CreateDirectory("Content\\custom_levels");
            }
            if (!File.Exists("Content\\custom_levels\\" + levelName + ".txt"))
            {
                FileStream f = File.Create("Content\\custom_levels\\" + levelName + ".txt");
                f.Dispose();
            }

            using (FileStream stream = File.Open(combined, FileMode.Open, FileAccess.Write, FileShare.ReadWrite))
            {

                using (StreamWriter sw = new StreamWriter(stream))
                {
                    int startingX = 999999;
                    int endingX = -999999;
                    int startingY = 999999;
                    int endingY = -999999;
                    foreach (Tile tile in tiles)
                    {
                        if (tile.Position.X < startingX)
                        {
                            startingX = tile.Position.X;
                        }
                        if (tile.Position.X > endingX)
                        {
                            endingX = tile.Position.X;
                        }
                        if (tile.Position.Y < startingY)
                        {
                            startingY = tile.Position.Y;
                        }
                        if (tile.Position.Y > endingY)
                        {
                            endingY = tile.Position.Y;
                        }
                    }
                    for (int y = startingY; y <= endingY; y += 25)
                    {
                        for (int x = startingX; x <= endingX; x += 25)
                        {
                            bool flag_ = false;
                            foreach (Tile tile in tiles)
                            {
                                if (tile.Position.X == x && tile.Position.Y == y)
                                {
                                    flag_ = true;
                                    if (tile.isLight)
                                    {
                                        sw.Write("l" + tile.TileID + ",");
                                    }
                                    else if (tile.Attribute == "player_spawn")
                                    {
                                        sw.Write("p" + tile.TileID + ",");
                                    }
                                    else
                                    {
                                        sw.Write(tile.TileID + ",");
                                    }
                                    break;
                                }
                            }
                            if (!flag_)
                            {
                                sw.Write("-1,");
                            }
                        }
                        sw.WriteLine("");
                    }
                    sw.Close();
                }
                state = "place";
            }
            return null;
        }
        private void AddAttribute(String attr)
        {
            foreach (Tile tile in tiles)
            {
                if (tile.Attribute == attr)
                {
                    tile.Attribute = "";
                }
            }
            foreach (Tile tile in tiles)
            {
                if (tile.Position.X == currentPosition.X && tile.Position.Y == currentPosition.Y)
                {
                    tile.Attribute = attr;
                }
            }
        }
        private void Menu()
        {
            GameMenu menu = new GameMenu(game, GraphicsDevice, graphicsManager, spriteBatch, Content, menuStrings, new Vector2(5, game.Window.ClientBounds.Height / 2), 50);
            menu.OnSelect += new EventHandler(MenuSelectEvent);
        }
        private void MenuSelectEvent(object sender, EventArgs eventArgs)
        {
            String menuItem = ((GameMenu)sender).GetCurrentItem();
            ((GameMenu)sender).Destroy();
            if (menuItem.Equals("Exit"))
            {
                game.MainMenu();
                this.game.music.PlayBackgroundMusic(0);
            }
            else if (menuItem.Equals("Load"))
            {
                String[] levelFilePaths = Directory.GetFiles("Content\\custom_levels");
                List<String> myLevelsMenuStrings = new List<String>();
                foreach (String str in levelFilePaths)
                {
                    myLevelsMenuStrings.Add(str.Split('\\')[str.Split('\\').Length - 1]);
                }
                GameMenu myLevelsMenu = new GameMenu(game, GraphicsDevice, graphicsManager, spriteBatch, Content, myLevelsMenuStrings, 
menuPosition, 50);
                myLevelsMenu.OnSelect += new EventHandler(MyLevelsMenuSelectEvent);
            }
            else if (menuItem.Equals("Save"))
            {
                String levelName = Microsoft.VisualBasic.Interaction.InputBox("Level Editor", "Enter your level name:", loadedLevelName);
                SaveLevel(levelName);
            }
            else if (menuItem.Equals("Block"))
            {
                GameMenu gameMenu = new GameMenu(game, graphics, graphicsManager, spriteBatch, Content, blockMenuStrings, menuPosition, 50);
                gameMenu.OnSelect += new EventHandler(BlockMenuSelectEvent);
            }
            else if (menuItem.Equals("- Back"))
            {
                state = "place";
            }
        }
        private void MyLevelsMenuSelectEvent(object sender, EventArgs eventArgs)
        {
            String menuItem = ((GameMenu)sender).GetCurrentItem();
            ((GameMenu)sender).Destroy();
            LevelLoader ll = new LevelLoader(graphicsManager, tileSprite, "Content\\custom_levels\\" + menuItem);
            tiles = ll.Load();
            state = "choose";
            loadedLevelName = menuItem.Replace(".txt", "");
        }
        private void BlockMenuSelectEvent(object sender, EventArgs eventArgs)
        {
            String menuItem = ((GameMenu)sender).GetCurrentItem();
            ((GameMenu)sender).Destroy();
            foreach (Tile tile in tiles)
            {
                if (tile.Position.X == currentPosition.X && tile.Position.Y == currentPosition.Y)
                {
                    if (menuItem.Equals("Add Lamp"))
                    {
                        tile.isLight = true;
                    }
                    else if(menuItem.Equals("Remove Lamp"))
                    {
                        tile.isLight = false;
                    }
                    else if (menuItem.Equals("Player Spawn"))
                    {
                        AddAttribute("player_spawn");
                    }
                    else if (menuItem.Equals("Enemy Spawn"))
                    {
                        AddAttribute("enemy_spawn");
                    }
                    else if (menuItem.Equals("- Back"))
                    {
                        state = "place";
                    }
                }
            }
            state = "place";
        }
    }
}
