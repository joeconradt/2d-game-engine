﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DGameEngine
{
    class Rectangle2
    {
        public float X = 0;
        public float Y = 0;
        public float Width = 0;
        public float Height = 0;
        public Rectangle2(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }
    }
}
