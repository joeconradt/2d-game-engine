﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _2DGameEngine
{
    class SolidObject : GameObject
    {
        public bool Destructible
        {
            get;
            set;
        }
        public int HitPoints
        {
            get;
            set;
        }
        public SolidObject(GraphicsDeviceManager graphics, Texture2D sprite, Rectangle position)
            : base(graphics, sprite, position)
        {

        }
    }
}
